/* Given a string, find the length of the longest substring without repeating characters.
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 * Tests
 * ""
 * "aab"
 * "dvdf"
 * "aaaaaaaaaaaab"
 * "1234"
 * "abcabcbb"
 * "bbbbb"
 * "pwwkew"
 */

public class Solution {
    public int lengthOfLongestSubstring(String s) {
        final Map<Character, Integer> pos = new HashMap();
        int length = 0;
        int curLength = 0;
        for (int i = 0; i < s.length(); i++) {
            final Integer p = pos.put(s.charAt(i), i);
            if (p == null) curLength++;
            else if (i - p <= curLength) {
                length = Math.max(length, curLength);
                curLength = i - p;
            } else curLength++;
        }
        return Math.max(length, curLength);
    }
}
