public class Solution {
    public List<List<Integer>> threeSum_1(int[] nums) {
		// time exceeded
        final Set<List<Integer>> set = new HashSet(); 
        for (int x = 0; x < nums.length; x++) {
            for (int y = x + 1; y < nums.length; y++) {
                for (int z = y + 1; z < nums.length; z++) {
                    if (nums[x] + nums[y] + nums[z] == 0) {
                        Integer[] r = new Integer[]{nums[x], nums[y], nums[z]};
                        Arrays.sort(r);
                        set.add(Arrays.asList(r));
                    }
                }
            }
        }
        return new LinkedList<List<Integer>>(set);
    }

    public List<List<Integer>> threeSum_2(int[] nums) {
        // time exceeded
		final Map<Integer, Set<Integer>> acc = new TreeMap();
        for (int i = 0; i < nums.length; i++) {
            Set<Integer> s = acc.get(nums[i]);
            if (s == null) s = new HashSet();
            s.add(i);
            acc.put(nums[i], s);
        }
        final Set<List<Integer>> t = new HashSet();
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                final int n = -(nums[i] + nums[j]);
                final Set<Integer> s = acc.get(n);
                final Integer[] tt = new Integer[]{nums[i], nums[j], n};
                Arrays.sort(tt);
                if (s != null && 
                        (nums[i] == nums[j] && nums[i] != n && s.size() > 0 
                            || nums[i] == nums[j] && nums[i] == n && s.size() > 2
                            || nums[i] != nums[j] && nums[i] != n && nums[j] != n && s.size() > 0)
                    )
                    t.add(Arrays.asList(tt));
            }   
        }
        return new LinkedList<List<Integer>>(t);
    }
}
