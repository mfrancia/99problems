public class Solution {
    public int reverse(int x) {
        long acc = 0;
        for (; x != 0;) {
            acc = acc * 10 + (x % 10);
            x /= 10;
        }
        return Math.abs(acc) > Integer.MAX_VALUE? 0: (int)acc;
    }
}
