/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
/*
 * TESTS
 * []
 * [0]
 * [2,4,3]
 * [5,6,4]
 * [1,2,4]
 * [4,5,6]
 * [1,2,3]
 * [0]
 * [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]
 * [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]
 */
public class Solution {
    /* SUFFERS ON INTEGER OVERFLOW
	   public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        double acc = myiterate(l1) + myiterate(l2);
        final ListNode res = new ListNode(-1);
        ListNode tmp = res;
        while((long)acc > 0) {
            tmp.next = new ListNode((int)(acc % 10)); // 123 -> 3
            tmp = tmp.next;
            acc /= 10; // 123 -> 12
        }
        return res.next;
    }
    private double myiterate(ListNode l) {
        if (l == null) l = new ListNode(0); // check it
        double r = l.val;
        ListNode acc = l.next;
        double i = 10;
        while(acc != null) { 
            r += acc.val * i; // increment r1
            acc = acc.next; // next element
            i *= 10;
        }
        return r;
    }*/
    private int sum(int a, int b, int c) { return (a+b+c)%10; }
    private int sumList(ListNode a, ListNode b, int c) { return sum(check(a), check(b), c); }
    private int carriage(int a, int b, int c) { return c+a+b>=10?1:0; }
    private int carriageList(ListNode a, ListNode b, int c) { return carriage(check(a), check(b), c); }
    private int check(ListNode l) { return l != null? l.val: 0; }
    private ListNode next(ListNode l) { return l != null? l.next: null; }
    
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carriage = carriageList(l1, l2, 0);
        final ListNode res = new ListNode(sumList(l1, l2, 0));
        ListNode tmp = res;
        l1 = next(l1);
        l2 = next(l2);
        while(l1 != null || l2 != null) {
            tmp.next = new ListNode(sumList(l1, l2, carriage)); 
            tmp = tmp.next;
            carriage = carriageList(l1, l2, carriage);
            l1 = next(l1); l2 = next(l2);
        }
        if (carriage == 1) tmp.next = new ListNode(1);
        return res;
    }
}
