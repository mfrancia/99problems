/*
 * Test strings
 * "-9223372036854775809"
 * "9223372036854775809"
 * "-2147483649"
 * "-2147483648"
 * "2147483648"
 * "\\r\\n1"
 * "+-1"
 * "+1"
 * "/1"
 * "*2"
 * "-"
 * "a20"
 * ""
 * "0"
 * "1"
 * "-123"
 * "a"
 * "    010   "
 * "    0.10   "
 * "    0.10]   "
 * "  a20"
 * "  1a20"
 * "  1 20"
 */
public class Solution {
    public int myAtoi(String str) {
        String acc = "";
        int i = 0;
        str = str.trim();
        for(;i<str.length()&&(str.charAt(i)=='-'||str.charAt(i)=='+'||Character.isDigit(str.charAt(i)));i++) 
                acc += str.charAt(i);
        try { 
            double res = Double.parseDouble(acc);
            if (res > Integer.MAX_VALUE) { return Integer.MAX_VALUE; }
            else if (res < Integer.MIN_VALUE) { return Integer.MIN_VALUE; }
            return (int) res;
        } catch (Exception ex) { return 0; }
    }
}
