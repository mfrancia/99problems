public class Solution {
	public int[] twoSum_quadratic(int[] nums, int target) {
        for(int x = 0; x < nums.length; x++) {
            for(int y = 0; y < nums.length; y++) {
                if (x != y && nums[x] + nums[y] == target)
					return new int[]{x, y};
            }   
        }
        throw new IllegalStateException("Error!");
    }
	public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap();
        for(int x = 0; x < nums.length; x++) {
            if(map.containsKey(target - nums[x])) 
				return new int[]{map.get(target-nums[x]), x};
            map.put(nums[x], x);
        }
        throw new IllegalStateException("Error!");
	}
}
