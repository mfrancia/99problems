package w4bo.ninetynineproblems

import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TestSpec extends FlatSpec {
  "S05" should "reverse a list" in {
    assert(S05.reverse(List(1, 2, 3, 4)).equals(List(4, 3, 2, 1)))
    assert(S05.reverse(List()).equals(List()))
  }

  "S06" should "test if a string is palindrome" in {
    assert(S06.isPalindrome(List(1, 2, 3, 2, 1)))
    assert(!S06.isPalindrome(List(0, 1, 2, 3, 2, 1)))
    assert(S06.isPalindrome(List()))
  }

  "S07" should "Flatten a nested list structure" in {
    assert(S07.flatten(List(5)).equals(List(5)))
    assert(S07.flatten(List(1, List(2, List(3, List(4, 5))))).equals(List(1, 2, 3, 4, 5)))
  }

  "S08" should "Eliminate consecutive duplicates of list elements. " in {
    assert(S08.compress(List("a", "a", "a", "a", "b", "c", "c", "a", "a", "d", "e", "e", "e", "e")).equals(List("a", "b", "c", "a", "d", "e")))
  }

  "S09" should "" in {
    assert(packSubsequence 1 [1,1,2,2] == ([1,1], [2,2]) &&
    assert(packSubsequence 1 [1] == ([1], []) &&
    pack "anna" == [['a'],['n', 'n'], ['a']] &&
    pack [1,1,2,2] == [[1, 1], [2, 2]]
  }
}
