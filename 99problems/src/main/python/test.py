import unittest
from . import p01
class Tests(unittest.TestCase):

    def test_01(self):
        self.assertEqual(p01.myLast([1, 2, 3]), 3)
        self.assertEqual(p01.myLast([1]), 1)
        self.assertEqual(p01.myLast([]), [])

        self.assertEqual(p01.myLast2([1, 2, 3]), 3)
        self.assertEqual(p01.myLast2([1]), 1)
        self.assertEqual(p01.myLast2([]), [])

if __name__ == '__main__':
    unittest.main()