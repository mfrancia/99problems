# Find the last element of a list
def myLast(l):
    if (len(l) == 0):
        return []
    elif len(l) == 1:
        return l[0]
    else:
        return myLast(l[1:])

def myLast2(l):
    if len(l) == 0:
        return []
    return l[-1]