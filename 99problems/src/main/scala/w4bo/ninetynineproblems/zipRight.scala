package w4bo.ninetynineproblems

object S06v2 {
  def zipRight[A](l: List[A]): List[(A, Int)] = l match {
    case x :: xs => (x, 0) :: zipRightCount(xs, 1)
    case Nil => Nil
  }

  def zipRightCount[A](l: List[A], i: Int): List[(A, Int)] = (l, i) match {
    case (x :: xs, i) => (x, i) :: zipRightCount(xs, i + 1)
    case (Nil, _) => Nil
  }

  def main(args: Array[String]): Unit = {
    print(zipRight(List(10, 20, 30)))
    print(zipRight(List(10)))
    print(zipRight(List()))
  }
}
