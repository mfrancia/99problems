package w4bo.ninetynineproblems

object S08 {
  def compress[A](l: List[A], prev: Option[A] = None): List[A] = l match {
    case x :: xs => if (prev.nonEmpty && x == prev.get) compress(xs, prev) else x :: compress(xs, Option(x))
    case _ => List()
  }
}
