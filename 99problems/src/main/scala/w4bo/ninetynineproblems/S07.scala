package w4bo.ninetynineproblems

object S07 {
  def flatten(l: List[Any]): List[Any] = l match {
    case x :: xs => (if (x.isInstanceOf[List[Any]]) flatten(x.asInstanceOf[List[Any]]) else List(x)) ::: flatten(xs)
    case List() => List()
  }
}
