# 99 (programming) problems

The problems are gathered from the following websites:

* [http://www.ic.unicamp.br/~meidanis/courses/mc336/2009s2/prolog/problemas/]
* [http://aperiodic.net/phil/scala/s-99/]
* [https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems]

## w4bo.ninetynineproblems.Test

The correctness of every solution is verifiable through the invocation of test function.

To test scala and java code run
```
./gradlew
```

To test python code
```
python -m unittest src/main/python/test.py
```

## Setup of the environments

### Haskell
```
sudo apt-get install haskell-platform hlint
ghci p__.hs
```
### Prolog

* [http://www.swi-prolog.org/build/Debian.html]
```
swipl
consult('p__.pl')
```

## Problems

* P01 Find the last element of a list.
* P02 Find the last but one element of a list.
* P03 Find the K'th element of a list.
* P04 Find the number of elements of a list.
* P05 Reverse a list.
* P06 Find out whether a list is a palindrome.
* P07 Flatten a nested list structure.
* P08 Eliminate consecutive duplicates of list elements.
* P09 Pack consecutive duplicates of list elements into sublists.
* P10 Run-length encoding of a list.
* P11 Modified run-length encoding.
* P12 Decode a run-length encoded list.
* P13 Run-length encoding of a list (direct solution).
* P14 Duplicate the elements of a list.
